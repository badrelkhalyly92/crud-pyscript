package com.product.pyscriptWS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.product.pyscriptWS.entity.Book;
import com.product.pyscriptWS.entity.Size;
import com.product.pyscriptWS.entity.Type;

@RestController
@CrossOrigin
public class BookController {
	
	@Autowired
	BookRepository Book;
	
	@Autowired
	SizeRepository size;
	
	@Autowired
	TypeRepository type;
	
	@RequestMapping(value = "/listOfBooks")
	   public ResponseEntity<List<Book>> getProduct() { 
	      return new ResponseEntity<>(Book.findAll(), HttpStatus.OK);
	}
	
	 private Sort.Direction getSortDirection(String direction) {
		    if (direction.equals("asc")) {
		      return Sort.Direction.ASC;
		    } else if (direction.equals("desc")) {
		      return Sort.Direction.DESC;
		    }

		    return Sort.Direction.ASC;
		  }
	
	@GetMapping("/books")
	  public ResponseEntity<Map<String, Object>> getAllBooksPage(
	      @RequestParam(required = false) String title,
	      @RequestParam(required = false,defaultValue = "0") int page,
	      @RequestParam(required = false,defaultValue = "3") int size,
	      @RequestParam(defaultValue = "id,desc") String[] sort) {

	    try {
	      List<Order> orders = new ArrayList<Order>();

	      if (sort[0].contains(",")) {
	        // will sort more than 2 fields
	        // sortOrder="field, direction"
	        for (String sortOrder : sort) {
	          String[] _sort = sortOrder.split(",");
	          orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
	        }
	      } else {
	        // sort=[field, direction]
	        orders.add(new Order(getSortDirection(sort[1]), sort[0]));
	      }

	      List<Book> books = new ArrayList<Book>();
	      Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

	      Page<Book> pageTuts;
	      if (title == null)
	        pageTuts = Book.findAll(pagingSort);
	      else
	        pageTuts = Book.findByTitleContaining(title, pagingSort);

	      books = pageTuts.getContent();

	      Map<String, Object> response = new HashMap<>();
	      response.put("books", books);
	      response.put("currentPage", pageTuts.getNumber());
	      response.put("totalItems", pageTuts.getTotalElements());
	      response.put("totalPages", pageTuts.getTotalPages());

	      return new ResponseEntity<>(response, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	
	@RequestMapping(value = "/sizes")
	   public ResponseEntity<List<Size>> getSizes() { 
	      return new ResponseEntity<>(size.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/types")
	   public ResponseEntity<List<Type>> getTypes() { 
	      return new ResponseEntity<>(type.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/booksByIdTechnique/{id}", method = RequestMethod.GET)
	   public ResponseEntity<Book> findByIdTechnique(@PathVariable("id") int id) { 
	      return new ResponseEntity<Book>( Book.findOneByReference(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/books/{ref}", method = RequestMethod.DELETE)
	   public ResponseEntity<Object> delete(@PathVariable("ref") int ref) { 
		Book clothToDelete = Book.findById(ref).get();
		Book.delete(clothToDelete);
	      return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
	}
	
	@PostMapping(path = "books")
	public ResponseEntity<Book> create(@RequestBody Book newUser) throws Exception {
		Book BookAdd = Book.save(newUser);
	    if (BookAdd == null) {
	        throw new Exception();
	    } else {
	        return new ResponseEntity<>(BookAdd, HttpStatus.CREATED);
	    }
	}
	
	@PutMapping(path = "books")
	public ResponseEntity<Book> update(@RequestBody Book newUser) throws Exception {
		Book bookModif = Book.findOneByReference(newUser.getReference());
		bookModif.setPrice(newUser.getPrice());
		bookModif.setQty(newUser.getQty());
		bookModif.setTitle(newUser.getTitle());
		Book.save(bookModif);
	    return new ResponseEntity<>(bookModif, HttpStatus.CREATED);
	}
	
	
	@PostMapping(path = "createRoot")
	public ResponseEntity<Book> createRoot(@RequestBody Book newUser) throws Exception {
		Root<Book> bookAdd =null; 
		bookAdd.get("ds").get("dd");
		
		
		return null;
	}
	
	

}
