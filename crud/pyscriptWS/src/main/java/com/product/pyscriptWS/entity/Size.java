package com.product.pyscriptWS.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Size {
	@Id
    private int id;

    private String title;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Size(int id ) {
		super();
		this.id = id; 
	}

	public Size() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
    
    
}
