package com.product.pyscriptWS;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.product.pyscriptWS.entity.Size;

@RepositoryRestResource
public interface SizeRepository  extends JpaRepository<Size, Integer>{

}
