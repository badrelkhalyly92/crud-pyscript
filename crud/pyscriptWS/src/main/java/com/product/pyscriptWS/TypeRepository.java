package com.product.pyscriptWS;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.product.pyscriptWS.entity.Type;

@RepositoryRestResource
public interface TypeRepository  extends JpaRepository<Type, Integer> {

}
