package com.product.pyscriptWS;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.product.pyscriptWS.entity.Book; 

@RepositoryRestResource
public interface BookRepository extends JpaRepository<Book, Integer>{
	
	Book findOneByReference(int idTechnique);
	Page<Book> findByTitleContaining(String title, Pageable pagingSort);

}
