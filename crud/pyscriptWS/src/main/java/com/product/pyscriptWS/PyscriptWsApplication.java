package com.product.pyscriptWS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class PyscriptWsApplication implements CommandLineRunner {

	@Autowired
	BookRepository books;
	
	public static void main(String[] args) {
		SpringApplication.run(PyscriptWsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception { 
		
	}
	
	
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/book").allowedOrigins("*");
			}
		};
	}
	
	

}
