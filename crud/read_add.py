import json
import csv
from js import XMLHttpRequest,console, setInterval 
from pyodide.http import pyfetch
from pyodide import create_proxy
import asyncio
from urllib import parse 
try:
    import urlparse
    from urllib import urlencode
except: # For Python 3
    import urllib.parse as urlparse
    from urllib.parse import urlencode

#initiate defaut valeus
totalItems = 0
totalPages = 0
currentPage = 0

target = "http://localhost:9090/books"


async def displayInfo(*ags, **kws): 
    response = await pyfetch(url=target, method="GET")
    output = await response.json()

    global totalItems
    global totalPages
    global currentPage 

    totalItems= json.dumps(output['totalItems'])
    totalPages=json.dumps(output['totalPages'])
    currentPage=json.dumps(output['currentPage'])
    ul ='<li class="page-item"><a class="page-link" href="#">Previous</a></li>'
    #Element('paginationNav').write(ul)
    console.log(ul)
    for i in range(1,int(totalPages)+1):
        ul = ul + f'<li  class="page-item"><a id={i} class="page-link">{i}</a></li>'
    ul = ul + '<li class="page-item"><a class="page-link" href="#">Next</a></li>'
    Element('paginationNav').write(ul)

    
    
    document.getElementById("panel").innerHTML = ""
     
    for rows in output['books']: 
        panel = Element("panel").element
        divPanel = document.createElement("div")
        divPanel.setAttribute("class", "col-sm-4")
        panel.appendChild(divPanel); 

        divPanelContent = document.createElement("div")
        divPanelContent.setAttribute("class", "panel panel-primary")
        divPanel.appendChild(divPanelContent); 

        titlePanel= document.createElement("div")
        titlePanel.setAttribute("class", "panel-heading")
        titlePanel.innerHTML = rows['title']
        divPanelContent.appendChild(titlePanel); 

        titlePanel= document.createElement("div")
        titlePanel.setAttribute("class", "panel-body")
        titlePanel.innerHTML = "Price : " + str(rows['price']) + "<br> "+ "Quantity :"+ str(rows['qty'])+ "<br> "+ "Reference :"+ str(rows['reference'])
        divPanelContent.appendChild(titlePanel)

        footerPanel= document.createElement("div")
        footerPanel.setAttribute("class", "panel-footer")
        divPanelContent.appendChild(footerPanel)

        buttonDelete= document.createElement("button")
        buttonDelete.setAttribute("class", "btn btn-danger")
        buttonDelete.setAttribute("data-toggle", "modal")
        buttonDelete.setAttribute("data-target", "#"+str(rows['reference'])) 
        footerPanel.appendChild(buttonDelete)

        spanDelete = document.createElement("span")
        spanDelete.setAttribute("class", "glyphicon glyphicon-trash")
        buttonDelete.appendChild(spanDelete)

        modalUpdate= document.createElement("div")
        modalUpdate.setAttribute("class", "modal fade")
        modalUpdate.setAttribute("role", "dialog")
        modalUpdate.setAttribute("id", str(rows['reference']))     
        panel.appendChild(modalUpdate); 

        modalDialog= document.createElement("div")
        modalDialog.setAttribute("class", "modal-dialog")      
        modalUpdate.appendChild(modalDialog); 

        modalContent= document.createElement("div")
        modalContent.setAttribute("class", "modal-content")  
        modalDialog.appendChild(modalContent)    

        ConfirmMsg = document.createElement("h1") 
        ConfirmMsg.innerHTML= "Are you sure to delete this Book ?"
        modalContent.appendChild(ConfirmMsg)  

        form = document.createElement("form")
        form.setAttribute("action", "/deleteProd.html") 
        modalContent.appendChild(form)

        inputIdProd = document.createElement("input")
        inputIdProd.setAttribute("type", "hidden") 
        inputIdProd.setAttribute("id", "idProd")
        inputIdProd.setAttribute("name", "idProd")
        inputIdProd.setAttribute("value", str(rows['id']))
        form.appendChild(inputIdProd)

        inputSupprimer = document.createElement("input")
        inputSupprimer.setAttribute("type", "submit") 
        inputSupprimer.setAttribute("class","btn btn-primary") 
        inputSupprimer.setAttribute("value", "Confirm Deletion")
        form.appendChild(inputSupprimer) 
       

        formUpdate = document.createElement("form")
        formUpdate.setAttribute("action", "/modifyProd.html") 
        footerPanel.appendChild(formUpdate)

        inputUpdate = document.createElement("button")
        inputUpdate.setAttribute("type", "submit")
        inputUpdate.setAttribute("class", "btn btn-success") 
        formUpdate.appendChild(inputUpdate)

        spanUpdate = document.createElement("span")
        spanUpdate.setAttribute("class", "glyphicon glyphicon-edit")
        inputUpdate.appendChild(spanUpdate)

        inputModify = document.createElement("input")
        inputModify.setAttribute("type", "hidden") 
        inputModify.setAttribute("id", "inputModify")
        inputModify.setAttribute("name", "inputModify")
        inputModify.setAttribute("value", str(rows['reference']))
        formUpdate.appendChild(inputModify)

        

        
 
async def choosePage(evt):
      global target
      page = int(evt.target.id) - 1
      modify = parse.urlsplit(target).query.split('=')
      url_parts = list(urlparse.urlparse(target))
      query = dict(urlparse.parse_qsl(url_parts[4]))
      params = {}
      params['page']=page
      query.update(params)
      url_parts[4] = urlencode(query)

      target = urlparse.urlunparse(url_parts)
      await displayInfo()


async def chooseSize(evt):
      global target
      size = document.getElementById("pageSize").value
      target = f"http://localhost:9090/books?size={size}"
      await displayInfo()

document.getElementById("pageSize").addEventListener('change',create_proxy(globals()["chooseSize"]))

async def addPrix(*ags, **kws):
    prix = Element("prix").element.value
    code = Element("code").element.value
    title = Element("label").element.value
    quantity = Element("quantity").element.value 
    Element("close-modal").element.click()
    elementToAdd = {}
    elementToAdd["title"]=title
    elementToAdd["reference"]=code
    elementToAdd["price"]=prix
    elementToAdd["qty"]=quantity
    app_json = json.dumps(elementToAdd)
    response = await pyfetch(url="http://localhost:9090/books", method="POST",
    headers={"Content-Type":"application/json"},body=app_json)
    
     

    if response.ok:
        await displayInfo()
        console.log('affichage')
  

loop = asyncio.get_event_loop()
loop.run_until_complete(displayInfo())
#loop.run_until_complete(displayWithPagination())

#setInterval(create_proxy(displayInfo),1000)

document.getElementById("read").lastChild.remove()  


 