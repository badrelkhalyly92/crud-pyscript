from pyodide import create_proxy
from js import console,FormData,window
from pyodide.http import pyfetch

singleUploadForm = document.querySelector("#singleUploadForm")
singleFileUploadInput = document.querySelector('#singleFileUploadInput')

async def uploadSingleFile(file):
            formData = FormData.new()
            formData.append("file", file)
            console.log('file uploaded',file)
            response = await pyfetch(url="http://localhost:8080/uploadFile", method="POST",
            headers={"Content-Type":"multipart/form-data"},body=formData)
            if response.ok:
                console.log('file uploaded with success')
    
async def uploadFunc(evt):
                files = singleFileUploadInput.files
                console.log('file') 
                console.log(files.item(0)) 
                
                await uploadSingleFile(files.item(0))
                evt.preventDefault()   
             
console.log('singleUploadForm')
console.log(singleUploadForm)

singleUploadForm.addEventListener('submit',  create_proxy(uploadFunc), True)