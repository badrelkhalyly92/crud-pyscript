from js import XMLHttpRequest
import json 
from urllib import parse

url = document.URL

 
def displayModif(): 
    modify = parse.urlsplit(url).query.split('=')[1]
    req = XMLHttpRequest.new()
    req.open("GET", "http://localhost:9090/booksByIdTechnique/"+modify, False) 
    req.send(None)
    Element("prix").element.value=json.loads(str(req.response))['price']
    Element("code").element.value=json.loads(str(req.response))['reference']
    Element("label").element.value=json.loads(str(req.response))['title']
    Element("quantity").element.value=json.loads(str(req.response))['qty']
    
    
 
displayModif()


def modifPrix(*ags, **kws):
    prix = Element("prix").element.value
    code = Element("code").element.value
    title = Element("label").element.value
    quantity = Element("quantity").element.value 
    elementToAdd = {}
    elementToAdd["title"]=title
    elementToAdd["reference"]=code
    elementToAdd["price"]=prix
    elementToAdd["qty"]=quantity
    app_json = json.dumps(elementToAdd)
    req = XMLHttpRequest.new()
    req.open("PUT", "http://localhost:9090/books", True)
    req.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
    req.send(app_json)
    req.response 
    document.location.href='/read_add.html'